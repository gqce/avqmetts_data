import os
import numpy as np
from itertools import count
import matplotlib.pyplot as plt
# from matplotlib.ticker import AutoMinorLocator
from matplotlib.ticker import AutoMinorLocator, MaxNLocator
from matplotlib.legend import Legend
from mpl_toolkits.axes_grid1.inset_locator import inset_axes, InsetPosition


fsize, tsize = 14, 12.4
style = 'default'
leg_length = 1.25

plt.style.use(style)
plt.rcParams['text.usetex'] = True
plt.rcParams['font.size'] = fsize
plt.rcParams['legend.fontsize'] = tsize
plt.rcParams['legend.handlelength'] = leg_length
plt.rc('text.latex', preamble=r'\usepackage{amsmath}')

xsize, ysize = 7, 5.

##############################################################
# Binder cumulant vs. transverse field and inverse temperature
##############################################################

fig, axs = plt.subplots(2, 2, figsize=(xsize, ysize),
                        tight_layout=True,
                        )

# axins = []
# axins.append(inset_axes(axs[1, 0], width="40%", height="40%", loc=1))
# axins1.tick_params(axis="y", direction="in")
# axins1.tick_params(axis="x", direction="in", pad=-15)
# axins[2].tick_params(labelsize=11)

beta_vals = ["0.70", "1.70"]
# hx_vals = [-4.78, -5.58]
# # factor of 2 for H in terms of Pauli matrices
# gamma_vals = [-0.5 * x for x in hx_vals]
# hx_vals = ["{:.2f}".format(abs(x)) + "m" if x < 0 else
#            "{:.2f}".format(abs(x)) + "p" for x in hx_vals]
# gamma_vals = ["{:.2f}".format(x) for x in gamma_vals]

symbols = ["o", "s", "^"]
colors = ["blue", "green", "red"]

ysizes = [3, 3]
xsizes = [3, 4]
for i, beta in zip(count(), beta_vals):
    for j, Lx, Ly in zip(count(), xsizes, ysizes):
        # exact
        x = -0.5 * np.loadtxt(
            f"data/Lx{Lx}Ly{Ly}-exact-beta{beta}-binder_v_hx.dat", usecols=0)
        y = np.loadtxt(
            f"data/Lx{Lx}Ly{Ly}-exact-beta{beta}-binder_v_hx.dat", usecols=1)
        axs[0, i].plot(x, y, '--', color=colors[j])

        # x = np.loadtxt(
        #     f"data/Lx{Lx}Ly{Ly}-exact-hx{hx}-binder_v_beta.dat", usecols=0)
        # y = np.loadtxt(
        #     f"data/Lx{Lx}Ly{Ly}-exact-hx{hx}-binder_v_beta.dat", usecols=1)
        # axs[i, 1].plot(x, y, symbols[j], color=colors[j])

        # avqite
        # if Lx == 4 and beta == "0.70":
        #     continue
        x = -0.5 * np.loadtxt(
            f"data/Lx{Lx}Ly{Ly}-avqite-beta{beta}-m2_v_hx.dat", usecols=0)
        m2 = np.loadtxt(
            f"data/Lx{Lx}Ly{Ly}-avqite-beta{beta}-m2_v_hx.dat", usecols=1)
        m2_err = np.loadtxt(
            f"data/Lx{Lx}Ly{Ly}-avqite-beta{beta}-m2_v_hx.dat", usecols=2)
        m4 = np.loadtxt(
            f"data/Lx{Lx}Ly{Ly}-avqite-beta{beta}-m4_v_hx.dat", usecols=1)
        m4_err = np.loadtxt(
            f"data/Lx{Lx}Ly{Ly}-avqite-beta{beta}-m4_v_hx.dat", usecols=2)
        yerr = np.sqrt(m4_err**2 / (9. * m2**4) +
                       (4. * m4**2 * m2_err**2) / (9. * m2**6))
        y = 1. - m4 / (3. * m2**2)
        if i == 0:
            axs[0, i].errorbar(x, y, yerr=yerr, fmt=symbols[j],
                           color=colors[j], capsize=4., label=rf"$N={Lx}\times{Ly}$")
        else:
            axs[0, i].errorbar(x, y, yerr=yerr, fmt=symbols[j],
                           color=colors[j], capsize=4.,)

        if i and j:
            print(i, j, beta, Lx, Ly)

            x = -0.5 * np.loadtxt(
                f"data/Lx{Lx}Ly4-exact-beta{beta}-binder_v_hx.dat", usecols=0)
            y = np.loadtxt(
                f"data/Lx{Lx}Ly4-exact-beta{beta}-binder_v_hx.dat", usecols=1)
            axs[0, i].plot(x, y, '--', color="red")

            x = -0.5 * np.loadtxt(
                f"data/Lx{Lx}Ly4-avqite-beta{beta}-m2_v_hx.dat", usecols=0)
            m2 = np.loadtxt(
                f"data/Lx{Lx}Ly4-avqite-beta{beta}-m2_v_hx.dat", usecols=1)
            m2_err = np.loadtxt(
                f"data/Lx{Lx}Ly4-avqite-beta{beta}-m2_v_hx.dat", usecols=2)
            m4 = np.loadtxt(
                f"data/Lx{Lx}Ly4-avqite-beta{beta}-m4_v_hx.dat", usecols=1)
            m4_err = np.loadtxt(
                f"data/Lx{Lx}Ly4-avqite-beta{beta}-m4_v_hx.dat", usecols=2)
            yerr = np.sqrt(m4_err**2 / (9. * m2**4) +
                           (4. * m4**2 * m2_err**2) / (9. * m2**6))
            y = 1. - m4 / (3. * m2**2)
            axs[0, i].errorbar(x, y, yerr=yerr, fmt="d",
                               color="red", capsize=4., label=rf"$N={Lx}\times 4$")

        # x = np.loadtxt(
        #     f"data/Lx{Lx}Ly{Ly}-avqite-hx{hx}-m2_v_beta.dat", usecols=0)
        # m2 = np.loadtxt(
        #     f"data/Lx{Lx}Ly{Ly}-avqite-hx{hx}-m2_v_beta.dat", usecols=1)
        # m2_err = np.loadtxt(
        #     f"data/Lx{Lx}Ly{Ly}-avqite-hx{hx}-m2_v_beta.dat", usecols=2)
        # m4 = np.loadtxt(
        #     f"data/Lx{Lx}Ly{Ly}-avqite-hx{hx}-m4_v_beta.dat", usecols=1)
        # m4_err = np.loadtxt(
        #     f"data/Lx{Lx}Ly{Ly}-avqite-hx{hx}-m4_v_beta.dat", usecols=2)
        # yerr = np.sqrt(m4_err**2 / (9. * m2**4) +
        #                (4. * m4**2 * m2_err**2) / (9. * m2**6))
        # y = 1. - m4 / (3. * m2**2)
        # axs[i, 1].errorbar(x, y, yerr=yerr, fmt=symbols[j],
        #                    color=colors[j], capsize=4.)

# <<<<<<< HEAD
# x = np.loadtxt("data/exact-critical_line.dat", usecols=0)
# y = np.loadtxt("data/exact-critical_line.dat", usecols=1)
# axs[1, 0].plot(x, y, "--", color="black", label=r"$\textrm{ED}$")
# =======
x = np.loadtxt("data/exact_expansion_critical_line.dat", usecols=0)
y = np.loadtxt("data/exact_expansion_critical_line.dat", usecols=1)
axs[1, 0].plot(x, y, "--", color="black")
# >>>>>>> 62b6f185ea02d59ef12b935bf96571cebe2d9b92
# axs[1, 0].plot(3.05, 0.0, "*", color="red", markersize=10,
#               label=r"$N\rightarrow \infty$")
axs[1, 0].plot(0.0, 2.269185, "*", color="red", markersize=10)
axs[1, 0].plot(3.04438, 0, "D", color="red", markersize=6)
# axs[1, 0].errorbar(2.877, 1 / 1.7, xerr=0.032, fmt="o",
#                    color="blue", label=r"$\textrm{AVQMETTS}$", capsize=4.)
# axs[1, 0].errorbar(2.354, 10 / 7, xerr=0.053,
#                    fmt="o", color="blue", capsize=4.)
# axs[1, 0].plot(2.98, 1 / 1.7, "o",
axs[1, 0].plot([2.8766], [1/1.7], "o",
               color="blue", label=r"$\textrm{AVQMETTS}$")
axs[1, 0].plot([2.9782, 3.0753], [1/1.7]*2, "o",
               color="green")
# axs[1, 0].plot(2.354, 10 / 7, "o", color="blue")
axs[1, 0].plot(2.3468, 10 / 7, "o", color="blue")
# axs[1, 0].plot(2.97, 1 / 1.7, "x",
axs[1, 0].plot([2.8537], [1/1.7], "x",
               color="red", label=r"$\textrm{ED}$")
axs[1, 0].plot([2.9709, 3.0711], [1/1.7]*2, "x",
               color="yellow")
# axs[1, 0].plot(2.39, 10 / 7, "x", color="red")
axs[1, 0].plot(2.3878, 10 / 7, "x", color="red")

beta = beta_vals[0]
hx = "4.60m"
hx_val = hx[:-1] if hx[-1] == 'm' else hx[:-1]
exact_val = 0.5713449748358853
m2 = np.loadtxt(
    f"data/Lx{Lx}Ly{Ly}-avqite-beta{beta}-hx{hx}-m2.dat", usecols=0)
m4 = np.loadtxt(
    f"data/Lx{Lx}Ly{Ly}-avqite-beta{beta}-hx{hx}-m4.dat", usecols=0)
y = 1. - m4 / (3. * m2**2)
maxp = 16
y = [np.mean(y[:2**i]) for i in range(1, maxp)]
yerr = [np.std(y[:2**i]) / np.sqrt(2**i) for i in range(1, maxp)]
# yp = [np.mean(y[:i + 1]) for i in range(1, len(y))]
x = [2**i for i in range(1, maxp)]
# axs[1, 1].semilogx(x[:], y[:], 'o-')
axs[1, 1].errorbar(x, y, yerr=yerr, fmt="o-",
        color=colors[0], capsize=4., label=rf"$\beta={float(beta):.1f},\; h_x={float(hx_val)/2:.1f}$")
axs[1, 1].plot([0, 2**maxp], 2 * [exact_val], "--",
               color=colors[0])
axs[1, 1].set_xscale('log')

beta = beta_vals[1]
hx = "5.60m"
hx_val = hx[:-1] if hx[-1] == 'm' else hx[:-1]
exact_val = 0.4827789666912591
m2 = np.loadtxt(
    f"data/Lx{Lx}Ly{Ly}-avqite-beta{beta}-hx{hx}-m2.dat", usecols=0)
m4 = np.loadtxt(
    f"data/Lx{Lx}Ly{Ly}-avqite-beta{beta}-hx{hx}-m4.dat", usecols=0)
y = 1. - m4 / (3. * m2**2)
maxp = 16
y = [np.mean(y[:2**i]) for i in range(1, maxp)]
yerr = [np.std(y[:2**i]) / np.sqrt(2**i) for i in range(1, maxp)]
# yp = [np.mean(y[:i + 1]) for i in range(1, len(y))]
x = [2**i for i in range(1, maxp)]
# axs[1, 1].semilogx(x[:], y[:], 'o-')
axs[1, 1].errorbar(x, y, yerr=yerr, fmt="s-",
        color=colors[1], capsize=4., label=rf"$\beta={float(beta):.1f},\; h_x={float(hx_val)/2:.1f}$")
axs[1, 1].plot([0, 2**maxp], 2 * [exact_val], "--",
               color=colors[1])
axs[1, 1].set_xscale('log')

# set axes
for i in range(len(beta_vals)):
    axs[0, i].set(xlabel=rf"$h_x$")

axs[0, 0].set(ylabel=rf"$U_4$")
axs[0, 0].legend(frameon=False, loc="upper right")
axs[0, 1].legend(frameon=False, loc="upper right")
axs[0, 1].set_xticks([2.8, 2.9, 3.0, 3.1])
axs[1, 0].set(ylabel=rf"$T/J$", xlabel=rf"$h_x$")
axs[1, 0].set_ylim(ymax=2.5, ymin=-0.05)
axs[1, 0].set_xlim(left=-0.05, right=3.3)
axs[1, 0].legend(frameon=False, loc="best")
axs[1, 1].set(ylabel=rf"$U_4$", xlabel=rf"$S$")

axs[1, 0].set_xticks([i for i in range(4)])
axs[1, 1].set_xticks([10**(i + 1) for i in range(4)])
axs[1, 1].set_yticks([0.2, 0.4, 0.6])
axs[1, 1].legend(frameon=False, loc="best")

for i, _ in enumerate(beta_vals):
    axs[0, i].annotate(rf"$\beta={float(beta_vals[i]):.1f}$", xy=(0.15, 0.2),
                       xycoords='axes fraction')
# axs[1, 0].annotate(r"$\textrm{ED}$", xy=(0.3, 0.8),
#                    xycoords='axes fraction')
# axs[1, 1].annotate(rf"$\beta={beta}$", xy=(0.6, 0.65),
#                    xycoords='axes fraction')
# axs[1, 1].annotate(rf"$h_x = {hx_val}$", xy=(0.52, 0.4),
#                    xycoords='axes fraction')
axs[1, 1].annotate(rf"$N={Lx}\times{Ly}$", xy=(0.5, 0.55),
                   xycoords='axes fraction')

axs[0, 0].annotate(r"$\textrm{(a})$", xy=(-0.35, 0.95),
                   xycoords='axes fraction')
axs[0, 1].annotate(r"$\textrm{(b})$", xy=(-0.35, 0.95),
                   xycoords='axes fraction')
axs[1, 0].annotate(r"$\textrm{(c})$", xy=(-0.35, 0.95),
                   xycoords='axes fraction')
axs[1, 1].annotate(r"$\textrm{(d})$", xy=(-0.35, 0.95),
                   xycoords='axes fraction')

plt.savefig(f'plt_phase_transition.pdf', dpi=300,
             pad_inches=.1, bbox_inches='tight')
# Plt.savefig(f'plt_extra_curve.pdf', dpi=300,
#             pad_inches=.1, bbox_inches='tight')

plt.show()
