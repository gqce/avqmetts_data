import os
import numpy as np
from itertools import count
import matplotlib.pyplot as plt
# from matplotlib.ticker import AutoMinorLocator
from matplotlib.ticker import AutoMinorLocator, MaxNLocator
from matplotlib.legend import Legend
from mpl_toolkits.axes_grid1.inset_locator import inset_axes, InsetPosition


style = 'default'
plt.style.use(style)
plt.rcParams['text.usetex'] = True
plt.rc('text.latex', preamble=r'\usepackage{amsmath}')

xsize, ysize = 3, 1.6

##############################################################
# Binder cumulant vs. transverse field and inverse temperature
##############################################################

fig, axs = plt.subplots(1, 1, figsize=(xsize, ysize),
                        tight_layout=True,
                        )

x = np.loadtxt("data/exact_expansion_critical_line.dat", usecols=0)
y = np.loadtxt("data/exact_expansion_critical_line.dat", usecols=1)
# axs.plot(x, y, "--", color="black")
# axs[1, 0].plot([2.8766], [1/1.7], "o",
#                color="blue", label=r"$\textrm{AVQMETTS}$")
# axs.plot([2.9782, 3.0753], [1/1.7]*2, "o", color="green")
axs.plot([2.9782], [1/1.7], "o", color="red")
# axs[1, 0].plot(2.354, 10 / 7, "o", color="blue")
axs.plot(2.3468, 10 / 7, "o", color="purple")
# axs[1, 0].plot(2.97, 1 / 1.7, "x",
# axs[1, 0].plot([2.8537], [1/1.7], "x",
#                color="red", label=r"$\textrm{ED}$")
# axs[1, 0].plot([2.9709, 3.0711], [1/1.7]*2, "x",
#                color="yellow")
# axs[1, 0].plot(2.39, 10 / 7, "x", color="red")
# axs[1, 0].plot(2.3878, 10 / 7, "x", color="red")

# set axes
axs.set(xlabel=rf"$h_x$")

axs.fill_between(x, y, 0, color='green', alpha=.4)
axs.fill_between(x.tolist()+[4], y.tolist()+[0], 3, color='blue', alpha=.2)

axs.set(ylabel=rf"$T/J$", xlabel=rf"$h_x$")
axs.set_ylim(ymax=3, ymin=0.0)
axs.set_xlim(left=0, right=4)
# axs.legend(frameon=False, loc="best")

# axs.set_xticks([i for i in range(4)])
axs.tick_params(direction="in")

plt.savefig(f'plt_phase_diagram.pdf', dpi=300,
             pad_inches=.1, bbox_inches='tight')
# Plt.savefig(f'plt_extra_curve.pdf', dpi=300,
#             pad_inches=.1, bbox_inches='tight')

plt.show()
