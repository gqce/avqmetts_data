from scipy.interpolate import UnivariateSpline
import matplotlib.pyplot as plt
import numpy as np



def get_crossing(path1, path2, path1p=None, path2p=None,
        calc_u4=False, plt_check=False,
        ):
    data1 = np.loadtxt(path1)
    data2 = np.loadtxt(path2)
    if calc_u4:
        data1p = np.loadtxt(path1p)
        data2p = np.loadtxt(path2p)
        x1, m12 = -0.5*data1[:, 0], data1[:, 1]
        x2, m22 = -0.5*data2[:, 0], data2[:, 1]
        m14, m24 = data1p[:, 1], data2p[:, 1]
        y1 = 1 - m14 / (3*m12**2)
        y2 = 1 - m24 / (3*m22**2)
    else:
        x1, y1 = -0.5*data1[:, 0], data1[:, 1]
        x2, y2 = -0.5*data2[:, 0], data2[:, 1]

    spl1 = UnivariateSpline(x1, y1)
    spl2 = UnivariateSpline(x2, y2)
    xs = np.linspace(x1.min()-0.1, x1.max()+0.1, 1000)
    diff = np.abs(spl1(xs) - spl2(xs))
    print("Crossing at", xs[diff.argmin()], "with value ", diff.min())
    if plt_check:
        plt.plot(x1, y1, "o")
        plt.plot(xs, spl1(xs), "-")
        plt.plot(x2, y2, "+")
        plt.plot(xs, spl2(xs), "-")
        plt.show()

for beta in ["0.70", "1.70"]:
    print(f" exact at beta = {beta}")
    get_crossing(f"data/Lx3Ly3-exact-beta{beta}-binder_v_hx.dat",
            f"data/Lx4Ly3-exact-beta{beta}-binder_v_hx.dat")
    if beta == "1.70":
        print("at 3x3 and 4x4:")
        get_crossing(f"data/Lx3Ly3-exact-beta{beta}-binder_v_hx.dat",
                f"data/Lx4Ly4-exact-beta{beta}-binder_v_hx.dat")
        print("at 4x3 and 4x4:")
        get_crossing(f"data/Lx4Ly3-exact-beta{beta}-binder_v_hx.dat",
                f"data/Lx4Ly4-exact-beta{beta}-binder_v_hx.dat")

for beta in ["0.70", "1.70"]:
    print(f" exact at beta = {beta}")
    get_crossing(f"data/Lx3Ly3-avqite-beta{beta}-m2_v_hx.dat",
            f"data/Lx4Ly3-avqite-beta{beta}-m2_v_hx.dat",
            path1p=f"data/Lx3Ly3-avqite-beta{beta}-m4_v_hx.dat",
            path2p=f"data/Lx4Ly3-avqite-beta{beta}-m4_v_hx.dat",
            calc_u4=True,
            )

    if beta == "1.70":
        print("at 3x3 and 4x4:")
        get_crossing(f"data/Lx3Ly3-avqite-beta{beta}-m2_v_hx.dat",
                f"data/Lx4Ly4-avqite-beta{beta}-m2_v_hx.dat",
                path1p=f"data/Lx3Ly3-avqite-beta{beta}-m4_v_hx.dat",
                path2p=f"data/Lx4Ly4-avqite-beta{beta}-m4_v_hx.dat",
                calc_u4=True,
                )
        print("at 4x3 and 4x4:")
        get_crossing(f"data/Lx4Ly3-avqite-beta{beta}-m2_v_hx.dat",
                f"data/Lx4Ly4-avqite-beta{beta}-m2_v_hx.dat",
                path1p=f"data/Lx4Ly3-avqite-beta{beta}-m4_v_hx.dat",
                path2p=f"data/Lx4Ly4-avqite-beta{beta}-m4_v_hx.dat",
                calc_u4=True,
                )
