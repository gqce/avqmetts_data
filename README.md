# AVQMETTS data
Data souce and python scripts to generate the figures of the following publication:
* J. C. Getelina, N. Gomes, T. Iadecola, P. P. Orth, and Y.-X. Yao, Adaptive Variational Quantum Minimally Entangled Typical Thermal States for Finite Temperature Simulations, [SciPost Physics 15, 102 (2023).](https://scipost.org/SciPostPhys.15.3.102)
