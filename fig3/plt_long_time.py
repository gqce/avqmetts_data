# import os
import numpy as np
from scipy.interpolate import griddata
from itertools import count
import matplotlib.pyplot as plt
# from matplotlib.ticker import AutoMinorLocator
# from matplotlib.legend import Legend
# from matplotlib.cm import ScalarMappable


# Plot setup

fsize, tsize = 18, 16
style = 'default'
leg_length = 1.25

plt.style.use(style)
plt.rcParams['text.usetex'] = True
plt.rcParams['font.size'] = fsize
plt.rcParams['legend.fontsize'] = tsize
plt.rcParams['legend.handlelength'] = leg_length
plt.rc('text.latex', preamble=r'\usepackage{amsmath}')

xsize, ysize = 8, 5

fig, axs = plt.subplots(1, 1, figsize=(xsize, ysize))

E_ed = -1.741709790934255864e+01
E_z = -17.399092912953876
E_alt = -17.412567409571768
Dz = "{:.3f}".format(abs(E_ed - E_z))
Dalt = "{:.3f}".format(abs(E_ed - E_alt))
Lx, Ly = 14, 1
x = np.loadtxt(
    f"data-tfim/Lx{Lx}Ly{Ly}-avqite-beta2.00-E_v_step-zbasis.dat", usecols=0)
y = np.loadtxt(
    f"data-tfim/Lx{Lx}Ly{Ly}-avqite-beta2.00-E_v_step-zbasis.dat", usecols=1)
axs.plot(
    x, y, label=rf"$\textrm{{only z-basis; diff. to exact value}} = {Dz} $")

x = np.loadtxt(
    f"data-tfim/Lx{Lx}Ly{Ly}-avqite-beta2.00-E_v_step-alt.dat", usecols=0)
y = np.loadtxt(
    f"data-tfim/Lx{Lx}Ly{Ly}-avqite-beta2.00-E_v_step-alt.dat", usecols=1)
axs.plot(
    x, y, label=rf"$\textrm{{alt x and z; diff. to exact value}} = {Dalt}$")

x = [0, 100]
y = 2 * [E_ed]
axs.plot(x, y, "--", color="black")

axs.set(
    ylabel=r"$\langle \hat{{\mathcal{{H}}}} \rangle_{\boldsymbol{\theta}(\beta)}$")
axs.set(xlabel=r"\textrm{{thermal step}}")
axs.legend(frameon=False, loc='best')
axs.annotate(rf"$L_x={Lx},\;L_y={Ly}$", xy=(0.2, 0.65),
             xycoords='axes fraction')
axs.annotate(rf"$\beta=2.0$", xy=(0.7, 0.65),
             xycoords='axes fraction')
axs.annotate(r"$N_{\textrm{samples}}=240$", xy=(0.6, 0.15),
             xycoords='axes fraction')

plt.savefig(f'plt_long_time.pdf', dpi=300,
            pad_inches=.1, bbox_inches='tight')

plt.show()
