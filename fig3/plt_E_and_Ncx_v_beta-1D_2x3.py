import os
import numpy as np
from itertools import count
import matplotlib.pyplot as plt
# from matplotlib.ticker import AutoMinorLocator
from matplotlib.ticker import AutoMinorLocator, MaxNLocator
from matplotlib.legend import Legend
from mpl_toolkits.axes_grid1.inset_locator import inset_axes


fsize, tsize = 14, 12.4
style = 'default'
leg_length = 1.25

energy_levels_tfim = np.asarray(
        [-17.8628085407619, -17.75049094994073, -16.967092731935498, -16.860407214115174, -16.860407214114815, -16.093834388527934, -16.093834388527714, -16.093834388527362, -16.09383438852734, -16.01495599347007, -16.01495599346998, -15.970323478289696, -15.286822330287057, -15.286822330286988, -15.286822330286483, -15.286822330286448, -15.256531742504833, -15.256531742504832, -15.220576045120009, -15.124872257644485, -15.124872257644169, -15.12487225764416, -15.12487225764399,
            -14.623165020067638, -14.623165020067619, -14.586523511602165, -14.586523511601927, -14.586523511601632, -14.586523511601596, -14.413563986879158, -14.413563986879135, -14.41356398687898, -14.413563986878966, -14.366448006679462, -14.366448006679455, -14.366448006679414, -14.366448006679349, -14.32486023629312, -14.279421036998851, -14.234788521818952, -14.234788521818723, -14.146615478329906, -14.146615478329801, -14.028053839434994, -14.028053839434724, -14.028053839434632,
            -14.028053839434632, -13.850779301212855, -13.850779301212166, -13.750490949939511, -13.733081284242344, -13.733081284242118, -13.733081284242108, -13.733081284242042, -13.713265168194189, -13.713265168194184, -13.71326516819409, -13.713265168194077, -13.639417315114388, -13.63941731511437, -13.639417315114313, -13.63941731511424, -13.606551928638263, -13.520996786034434, -13.520996786034413, -13.520996786034413, -13.520996786034312, -13.517848178052622, -13.517848178052574,
            -13.517848178052533, -13.517848178052466, -13.476364270854093, -13.476364270854042, -13.440101796774915, -13.440101796774858, -13.44010179677485, -13.440101796774822, -13.389337301173919, -13.389337301173784, -13.256531742504631, -13.256531742504617, -13.256531742504594, -13.25653174250452, -13.15479549602739, -13.154795496027255, -13.154795496027253, -13.154795496027228, -12.96069556538704, -12.960695565387034, -12.960695565387, -12.960695565386855, -12.906253109953596,
            -12.906253109953587, -12.90625310995347, -12.906253109953438, -12.887630063597253, -12.887630063597218, -12.887630063597014, -12.88763006359697, -12.860407214114236])
energy_levels_mfim = np.asarray(
        [-23.832039923164075, -20.123116585791113, -20.035093638188858, -20.035093638188552, -19.79424153370359, -19.794241533702795, -19.457518217805678, -19.457518217804008, -19.092920860291013, -19.09292086028985, -18.766456786941017, -18.76645678694066, -18.53823265805596, -18.538232658055918, -18.45575417004067, -17.917539774739094, -17.87591914607626, -17.875919146076228, -17.759353722039034, -17.759353722038995, -17.589929987824256, -17.589929987823997, -17.3995113468698,
            -17.399511346869325, -17.224120868184308, -17.22412086818409, -17.09917131805554, -17.099171318055504, -17.05352304637378, -16.617526295195525, -16.569720285233007, -16.56972028523289, -16.512115294531867, -16.512115294531498, -16.406058789535013, -16.406058789534345, -16.30696081145967, -16.306960811459618, -16.230075993014147, -16.230075993014136, -16.214303336951605, -16.188773470497587, -16.18877347049747, -16.17733059468077, -16.094217488751656, -16.094217488751557,
            -15.861205237005292, -15.861205237005167, -15.732670048487366, -15.732670048487243, -15.723192366444941, -15.723192366444843, -15.515298563199378, -15.502054180651486, -15.5020541806514, -15.499769011129507, -15.499769011129413, -15.446928674191378, -15.446928674191339, -15.43211382859424, -15.432113828594098, -15.414855047203481, -15.237469606158987, -15.237469606158932, -15.22462272991167, -15.224622729911525, -15.174165455308916, -15.174165455308827, -15.134747680218995,
            -15.072511929213391, -15.07251192921332, -15.053640045995436, -15.0536400459954, -14.960437868685661, -14.960437868685421, -14.956808072429812, -14.9568080724297, -14.932337035028702, -14.9323370350287, -14.892365111533788, -14.8199688022139, -14.81996880221374, -14.751640694169355, -14.741343923594814, -14.727929469364314, -14.727929469364224, -14.692907537736819, -14.6929075377368, -14.676958917061054, -14.646589241283712, -14.646589241283575, -14.615611851661921,
            -14.615611851661908, -14.504380342650101, -14.50438034264999, -14.461351918824295, -14.461351918824256, -14.440589733645604, -14.440589733645504, -14.394315029274692])
energy_levels_tfim -= energy_levels_tfim[0]
energy_levels_mfim -= energy_levels_mfim[0]

plt.style.use(style)
plt.rcParams['text.usetex'] = True
plt.rcParams['font.size'] = fsize
plt.rcParams['legend.fontsize'] = tsize
plt.rcParams['legend.handlelength'] = leg_length
plt.rc('text.latex', preamble=r'\usepackage{amsmath}')

xsize, ysize = 9, 7

##############################################################
# Energy and number of CNOTs as a function of beta
##############################################################

fig, axs = plt.subplots(2, 3, figsize=(xsize, ysize),
                        tight_layout=True,
                        )

axins = []
axins.append(inset_axes(axs[0, 1], width="50%", height="50%", loc=1))
axins.append(inset_axes(axs[1, 1], width="50%", height="50%", loc=1))
axins.append(inset_axes(axs[1, 0], width="50%", height="50%", loc=1))
# axins[0].tick_params(axis="y", direction="in")
# axins[1].tick_params(axis="y", direction="in")
# axins[2].tick_params(labelsize=11)

pmdls = ["data-tfim", "data-mfim"]
mlbls = [rf"$\textrm{{1D TFIM}}$", rf"$\textrm{{1D MFIM}}$     $\beta=2$"]
symbols = ["o-", "s-", "^-"]
colors = ["blue", "green", "red"]
plot_label = [r"$(a)$", r"$(b)$", r"$(c)$", r"$(d)$",
              r"$(e)$", r"$(f)$"]

# Convergence of thermal energy

Lx = 14
beta = "2.00"
nsteps = [20, 10]
for i, p, ns, note in zip(count(), pmdls, nsteps, mlbls):
    x = np.loadtxt(f"{p}/Lx{Lx}Ly1-avqite-beta{beta}-E_v_step.dat", usecols=0)
    y = np.loadtxt(f"{p}/Lx{Lx}Ly1-avqite-beta{beta}-E_v_step.dat", usecols=1)
    yerr = np.loadtxt(
        f"{p}/Lx{Lx}Ly1-avqite-beta{beta}-E_v_step.dat", usecols=2)

    axs[i, 0].errorbar(x, y, yerr=yerr, color=colors[0],
                       fmt=symbols[0], label=rf"$\beta={float(beta)}$",
                       capsize=4.)

    x = [0, ns + 1]
    y = np.loadtxt(f"{p}/Lx{Lx}Ly1-exact-E_v_beta.dat", usecols=1)
    for j, dum in enumerate(np.loadtxt(f"{p}/Lx{Lx}Ly1-exact-E_v_beta.dat", usecols=0)):
        if abs(dum - float(beta)) < 1e-12:
            y = 2 * [y[j]]

    axs[i, 0].plot(x, y, "--", color="black",
                   label=None)  # rf"\textrm{{exact}}"

    if not i:
        x = np.loadtxt(
            f"{p}/Lx{Lx}Ly1-avqite-beta4.00-E_v_step.dat", usecols=0)
        y = np.loadtxt(
            f"{p}/Lx{Lx}Ly1-avqite-beta4.00-E_v_step.dat", usecols=1)
        yerr = np.loadtxt(
            f"{p}/Lx{Lx}Ly1-avqite-beta4.00-E_v_step.dat", usecols=2)

        axs[i, 0].errorbar(x, y, yerr=yerr, color=colors[1],
                           fmt=symbols[1], label=rf"$\beta=4.0$",
                           capsize=4.)

        x = [0, ns + 1]
        y = np.loadtxt(f"{p}/Lx{Lx}Ly1-exact-E_v_beta.dat", usecols=1)
        for j, dum in enumerate(np.loadtxt(f"{p}/Lx{Lx}Ly1-exact-E_v_beta.dat", usecols=0)):
            if abs(dum - 4.) < 1e-12:
                y = 2 * [y[j]]

        axs[i, 0].plot(x, y, "--", color="black",
                       label=None)  # rf"\textrm{{exact}}"

    if i == 0:
        axs[i, 0].set(
            ylabel=r"$\langle \hat{{\mathcal{{H}}}} \rangle_{\boldsymbol{\theta}(\beta)}$")
    axs[i, 0].set(xlabel=r"\textrm{{thermal step}}")
    axs[i, 0].xaxis.set_ticks([j for j in range(0, ns + 1, ns // 5)])
    axs[i, 0].yaxis.set_major_locator(MaxNLocator(4))
    axs[i, 0].set_xlim(left=0, right=ns + 0.25)
    # axs[0, i].legend(frameon=False, loc="center right")

    axs[i, 0].annotate(note, xy=(0.3 if i else 0.15, 0.15 if i else 0.3),
                       xycoords='axes fraction', fontsize=tsize)

axs[0, 0].annotate(r"$N=14$", xy=(0.55, 0.45),
                   xycoords='axes fraction')

xlist = [1]*len(energy_levels_tfim)
axins[2].scatter(xlist, energy_levels_tfim, marker="_", s=1000, linewidth=0.5)
xlist = [4]*len(energy_levels_mfim)
axins[2].scatter(xlist, energy_levels_mfim, marker="_", s=1000, linewidth=0.5)
axins[2].set_ylim(-0.5, 5.2)
axins[2].set_xlim(-1, 6)
axins[2].set_xticks([1,4])
axins[2].set_yticks([0,1,2,3,4,5])
axins[2].set_xticklabels(['TFIM', 'MFIM'])


for i, p in zip(count(), pmdls):
    x = np.loadtxt(f"{p}/Lx{Lx}Ly1-avqite-E_v_beta.dat", usecols=0)
    y = np.loadtxt(f"{p}/Lx{Lx}Ly1-avqite-E_v_beta.dat", usecols=1)
    yerr = np.loadtxt(f"{p}/Lx{Lx}Ly1-avqite-E_v_beta.dat",
                      usecols=2) / np.sqrt(len(y))

    axs[i, 1].errorbar(x, y, yerr=yerr, color=colors[0],
                       fmt=symbols[0], label=None,
                       capsize=4.)

    y_exct = np.loadtxt(f"{p}/Lx{Lx}Ly1-exact-E_v_beta.dat", usecols=1)
    y = abs(y - y_exct) / abs(y_exct)
    axins[i].semilogy(x, y, symbols[1], color=colors[1],
                      label=None)

    if i == 0:
        axs[i, 1].set(
            ylabel=r"$\langle \hat{{\mathcal{{H}}}} \rangle_{\boldsymbol{\theta}(\beta)}$")
    axs[i, 1].set(xlabel=rf"$\beta$")
    axs[i, 1].xaxis.set_ticks([1, 2, 3, 4])
    axs[i, 1].yaxis.set_major_locator(MaxNLocator(5))
    axins[i].xaxis.set_ticks([1, 2, 3, 4])
    axins[i].yaxis.set_ticks([1e-2, 1e-4, 1e-6])
    # axins[i].yaxis.set_label_position("right")
    # axins[i].yaxis.set_ticks_position("right")
    # axins[i].minorticks_off()
    # axins[i].set_ylabel(
    #     rf"$\Big| 1 - \left\langle \hat{{\mathcal{{H}}}} \right\rangle$"
    #     rf"$ \left\langle \hat{{\mathcal{{H}}}} \right\rangle_\textrm{{ED}}^{{-1}} \Big|$",
    #     fontsize=10)
    axins[i].set_ylabel(r'$\epsilon$')
    axins[i].tick_params(labelsize=11)
    # ylabel=rf"$\left\langle \hat{{\mathcal{{H}}}} \right\rangle$"
    # rf"$- \left\langle \hat{{\mathcal{{H}}}} \right\rangle_\textrm{{ED}}$")

# for cnt, p, m in zip(count(), pmdls, mlbls):
#     k = 0
#     for Lx in range(14, 15):
#         for mtd, lbl in zip(methods, labels_upr):
#             x = np.loadtxt(f"{p}/Lx{Lx}Ly1-{mtd}-E_v_beta.dat", usecols=0)
#             y = np.loadtxt(f"{p}/Lx{Lx}Ly1-{mtd}-E_v_beta.dat", usecols=1)
#             yerr = np.loadtxt(
#                 f"{p}/Lx{Lx}Ly1-{mtd}-E_v_beta.dat", usecols=2)
#             axs[0, cnt].errorbar(x, y, yerr=yerr, color=colors[k],
#                                  fmt=symbols[k], label=labels_upr[k],
#                                  capsize=4.)
#             k += 1

#     axs[0, cnt].set(xlabel=r'$\beta$')
#     axs[0, cnt].set(ylabel=r'$\left\langle H \right\rangle$')
#     axs[0, cnt].set_xlim(left=0.4)
#     axs[0, cnt].xaxis.set_minor_locator(AutoMinorLocator(2))
#     axs[0, cnt].yaxis.set_minor_locator(AutoMinorLocator(2))
#     axs[0, cnt].legend(frameon=False, loc='best')
#     axs[0, cnt].annotate(plot_label[cnt], xy=(0.2, 0.85),
#                          xycoords='axes fraction')
#     axs[0, cnt].annotate(m, xy=(0.5, 0.8),
#                          xycoords='axes fraction')

axs[0, 0].annotate(r"$\beta=2.0$", xy=(0.6, 0.85),
                     xycoords='axes fraction')

axs[0, 0].annotate(r"$\beta=4.0$", xy=(0.6, 0.2),
                     xycoords='axes fraction')

xsizes = [i for i in range(4, 22, 2)]
labels_cx = [rf"$\beta=1.0$", rf"$\beta=2.0$",
             rf"$\beta=4.0$"]
betas = [1.0, 2.0, 4.0]
for cnt, p, m in zip(count(), pmdls, mlbls):
    xx = np.empty((len(betas), len(xsizes)))
    yy = np.empty((len(betas), len(xsizes)))
    yyerr = np.empty((len(betas), len(xsizes)))
    for j, L in enumerate(xsizes):
        x = np.loadtxt(f"{p}/Lx{L}Ly1-avqite-Ncx_v_beta.dat", usecols=0)
        y = np.loadtxt(f"{p}/Lx{L}Ly1-avqite-Ncx_v_beta.dat", usecols=1)
        yerr = np.loadtxt(f"{p}/Lx{L}Ly1-avqite-Ncx_v_beta.dat", usecols=2)
        for i, b in enumerate(betas):
            pos = [k for k, dum in enumerate(x) if np.isclose(dum, b)][0]
            xx[i, j] = L
            yy[i, j] = y[pos]
            yyerr[i, j] = yerr[pos]

    for i, sym, clr in zip(count(), symbols, colors):
        lbl = None if cnt else labels_cx[i]
        axs[cnt, 2].errorbar(xx[i, :], yy[i, :], yerr=yyerr[i, :],
                             fmt=sym, color=clr, label=lbl, capsize=4.)

    axs[cnt, 2].set(xlabel=r'$L$')
    if cnt == 0:
        axs[cnt, 2].set(ylabel=r'$N_{cx}$')
    axs[cnt, 2].yaxis.set_major_locator(MaxNLocator(5))
    axs[cnt, 2].xaxis.set_ticks(xsizes[::2])
    axs[cnt, 2].legend(frameon=False, loc='best')
    axs[cnt, 2].set_ylim(bottom=0)


axs[0, 0].annotate("(a)", xy=(-0.35, 0.95),
                   xycoords='axes fraction')
axs[1, 0].annotate("(b)", xy=(-0.25, 0.95),
                   xycoords='axes fraction')
axs[ 0, 1].annotate("(c)", xy=(-0.35, 0.95),
                   xycoords='axes fraction')
axs[1, 1].annotate("(d)", xy=(-0.25, 0.95),
                   xycoords='axes fraction')
axs[0, 2].annotate("(e)", xy=(-0.35, 0.95),
                   xycoords='axes fraction')
axs[1, 2].annotate("(f)", xy=(-0.25, 0.95),
                   xycoords='axes fraction')

plt.tight_layout()

plt.savefig(f'plt_E_and_Ncx_v_beta-1D.pdf')

plt.show()
