import os
import numpy as np
from itertools import count
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator, MaxNLocator
from matplotlib.legend import Legend
from mpl_toolkits.axes_grid1.inset_locator import inset_axes


fsize, tsize = 14, 12.4
style = 'default'
leg_length = 1.25

plt.style.use(style)
plt.rcParams['text.usetex'] = True
plt.rcParams['font.size'] = fsize
plt.rcParams['legend.fontsize'] = tsize
plt.rcParams['legend.handlelength'] = leg_length

xsize, ysize = 7, 5

##############################################################
# Energy and ansatz params for a single thermal step with AVQITE
##############################################################

fig, axs = plt.subplots(2, 2, figsize=(xsize, ysize),
                        sharex=True, tight_layout=True,
                        )

# create inset of parent plot, bounded to the upper right corner
axins1 = inset_axes(axs[0, 0], width="35%", height="47%", loc=1)
axins1.tick_params(axis="y", direction="in")
axins1.tick_params(axis="x", direction="out")
# axins1.tick_params(axis="x", direction="in", pad=-15)

# turn ticklabels of insets off
# axins.tick_params(labelleft=False, labelbottom=False)

Lx = 14
dt = "0.02"
gse = -23.832039923163933
labels = ['X', "Z"]
# labels = [rf"$\left| \psi_x \right\rangle$",
#           rf"$\left| \psi_z \right\rangle$"]
colors = ["blue", "green"]
symbols = ["o-", "s-"]
basis = ['x', 'z']
for cnt, p, clr, smbl, lbl in zip(count(), basis, colors, symbols, labels):
    x = np.loadtxt(
        f"data-{p}basis/DeltaE_tevol_v_beta-Lx{Lx}Ly1-dt{dt}.dat", usecols=0)
    y = abs(np.loadtxt(
            f"data-{p}basis/DeltaE_tevol_v_beta-Lx{Lx}Ly1-dt{dt}.dat", usecols=1))

    y_abs = y

    axs[0, 0].semilogy(x, y, smbl, color=clr, label=None)

    x = np.loadtxt(
        f"data-{p}basis/DeltaE_v_beta-Lx{Lx}Ly1-dt{dt}.dat", usecols=0)
    y = np.loadtxt(
        f"data-{p}basis/DeltaE_v_beta-Lx{Lx}Ly1-dt{dt}.dat", usecols=1)
    y += gse
    axins1.plot(x, y, smbl,
                color=clr,
                label=None,
                )
    print("max abs err:", y_abs.max(), "max rel err:", (y_abs/abs(y)).max())

    x = np.loadtxt(
        f"data-{p}basis/infid_v_beta-Lx{Lx}Ly1-dt{dt}.dat", usecols=0)
    y = np.loadtxt(
        f"data-{p}basis/infid_v_beta-Lx{Lx}Ly1-dt{dt}.dat", usecols=1)
    axs[0, 1].semilogy(x, y, smbl,
                       color=clr,
                       label=None,
                       )

    x = np.loadtxt(
        f"data-{p}basis/Nparam_v_beta-Lx{Lx}Ly1-dt{dt}.dat", usecols=0)
    y = np.loadtxt(
        f"data-{p}basis/Nparam_v_beta-Lx{Lx}Ly1-dt{dt}.dat", usecols=1)
    axs[1, 0].plot(x, y, smbl,
                   color=clr,
                   label=lbl)

    x = np.loadtxt(
        f"data-{p}basis/Ncx_v_beta-Lx{Lx}Ly1-dt{dt}.dat", usecols=0)
    y = np.loadtxt(
        f"data-{p}basis/Ncx_v_beta-Lx{Lx}Ly1-dt{dt}.dat", usecols=1)
    axs[1, 1].plot(x, y, smbl,
                   color=clr,
                   label=lbl)

# axs[0, 0].set(xlabel=r'$\beta$')
# axs[0, 0].set(ylabel=r'$\langle \phi[\mathbf{\theta(\beta)}]|\hat{H}\,|\phi[\mathbf{\theta(\beta)}] \rangle -E_{GS}$')
axs[0, 0].set(ylabel=r'$\left| E_i [\mathbf{\theta(\tau)}] -E_i(\tau) \right |$')
# axs[0, 0].set_ylim(ymin=1e-12)
axs[0, 0].xaxis.set_ticks([1, 2, 3, 4])
# axs[0, 0].xaxis.set_minor_locator(AutoMinorLocator(2))
# axs[0, 0].yaxis.set_minor_locator(AutoMinorLocator(5))

axs[0, 0].set_ylim(ymax=50)
axs[0, 0].yaxis.set_ticks([1e-1, 1e-3, 1e-5])

axins1.plot([0, 4], 2 * [gse], ":", color="black"),
#            label=rf"$E_\textrm{{GS}}$")
axins1.set_ylabel(r'$E_i[\mathbf{\theta(\tau)}]$', fontsize=11)

axins1.annotate(r'$E_{GS}$', xy=(0.5, 0.15),
        xycoords='axes fraction', fontsize=11)

# axins1.set_ylim(ymax=-10)
# axins1.yaxis.set_label_position("right")
# axins1.yaxis.set_ticks_position("right")
axins1.xaxis.set_ticks([1, 2, 3, 4])
axins1.yaxis.set_ticks([-17, -19, -21, -23])
# axins1.xaxis.set_ticks_position("top")
# axins1.yaxis.set_major_locator(MaxNLocator(4))
axins1.tick_params(labelsize=11)
# axins1.legend(frameon=False, loc="center right", fontsize=11)

# axins.xaxis.set_minor_locator(AutoMinorLocator(2))
# axs[0, 0].legend(frameon=False, loc='best')

# axs[0, 1].set(xlabel=r'$\beta$')
axs[0, 1].set(ylabel=r'$1 - \mathcal{F}$')
axs[0, 1].set_ylim(ymax=1e-3)
axs[0, 1].yaxis.set_ticks([1e-3, 1e-4, 1e-5, 1e-6])
# axs[0, 1].yaxis.set_major_locator(MaxNLocator(4))
# axs[0, 1].xaxis.set_minor_locator(AutoMinorLocator(2))

axs[1, 0].set(xlabel=r'$\tau$', ylabel=r'$N_\mathbf{\theta}$')
axs[1, 0].yaxis.set_major_locator(MaxNLocator(5))
axs[1, 1].legend(frameon=True, loc="center")
# axs[1, 0].xaxis.set_minor_locator(AutoMinorLocator(2))
# axs[1, 0].yaxis.set_minor_locator(AutoMinorLocator(2))
# axs[1, 0].legend(frameon=False, loc='best')

axs[1, 1].set(xlabel=r'$\tau$', ylabel=r'$N_{cx}$')
axs[1, 1].yaxis.set_major_locator(MaxNLocator(5))
# axs[1, 1].xaxis.set_minor_locator(AutoMinorLocator(2))
# axs[1, 1].yaxis.set_minor_locator(AutoMinorLocator(2))
axs[1, 1].legend(frameon=False, loc='lower left', bbox_to_anchor=(0.55, 0.4))

import itertools
for i, j in itertools.product(range(2), repeat=2):
    axs[i, j].axvline(x=2, ls=":")

axs[1, 0].annotate(rf'$N={Lx}$', xy=(0.55, 0.6),
                   xycoords='axes fraction')
axs[1, 0].annotate(rf'$\delta \tau={dt}$', xy=(0.55, 0.45),
                   xycoords='axes fraction')

box_labels = [r"\textrm{{(a)}}", r"\textrm{{(b)}}",
              r"\textrm{{(c)}}", r"\textrm{{(d)}}"]
axs[0, 0].annotate(box_labels[0], xy=(-0.3, 0.95),
                   xycoords='axes fraction')
axs[0, 1].annotate(box_labels[1], xy=(-0.3, 0.95),
                   xycoords='axes fraction')
axs[1, 0].annotate(box_labels[2], xy=(-0.3, 0.95),
                   xycoords='axes fraction')
axs[1, 1].annotate(box_labels[3], xy=(-0.3, 0.95),
                   xycoords='axes fraction')
plt.tight_layout()

plt.savefig(f'single_thermal_step-Lx{Lx}Ly1-dt{dt}.pdf')
# plt.savefig(f'single_thermal_step-Lx{Lx}Ly1-dt{dt}.pdf', dpi=300,
#             pad_inches=.1, bbox_inches='tight')


plt.show()
