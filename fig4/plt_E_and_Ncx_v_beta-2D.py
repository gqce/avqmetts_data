import os
import numpy as np
from itertools import count
import matplotlib.pyplot as plt
# from matplotlib.ticker import AutoMinorLocator
from matplotlib.ticker import AutoMinorLocator, MaxNLocator
from matplotlib.legend import Legend
from mpl_toolkits.axes_grid1.inset_locator import inset_axes, InsetPosition
plt.rcParams['text.usetex'] = True


fsize, tsize = 14, 12.4
style = 'default'
leg_length = 1.25

plt.style.use(style)
plt.rcParams['text.usetex'] = True
plt.rcParams['font.size'] = fsize
plt.rcParams['legend.fontsize'] = tsize
plt.rcParams['legend.handlelength'] = leg_length
plt.rc('text.latex', preamble=r'\usepackage{amsmath}')

xsize, ysize = 7, 7.5

energy_levels_tfim = np.loadtxt("data-tfim/Lx4Ly3-exact-spectrum.dat")
energy_levels_tfim -= energy_levels_tfim[0]
energy_levels_mfim = np.loadtxt("data-mfim/Lx4Ly3-exact-spectrum.dat")
energy_levels_mfim -= energy_levels_mfim[0]

##############################################################
# Energy and number of CNOTs as a function of beta
##############################################################

fig, axs = plt.subplots(3, 2, figsize=(xsize, ysize),
                        tight_layout=True,
                        )

axins = []
axins.append(inset_axes(axs[1, 0], width="50%", height="50%", loc=1))
axins.append(inset_axes(axs[1, 1], width="50%", height="50%", loc=1))
axins.append(inset_axes(axs[0, 1], width="50%", height="50%", loc=1))
# axins1.tick_params(axis="y", direction="in")
# axins1.tick_params(axis="x", direction="in", pad=-15)
axins[2].tick_params(labelsize=11)

pmdls = ["data-tfim", "data-mfim"]
mlbls = [rf"$\textrm{{2D TFIM}}$", rf"$\textrm{{2D MFIM}}$     $\beta=1$"]
symbols = ["o-", "s-", "^-"]
colors = ["blue", "green", "red"]
plot_label = [r"$(a)$", r"$(b)$", r"$(c)$", r"$(d)$",
              r"$(e)$", r"$(f)$"]

# Convergence of thermal energy

Lx = 4
Ly = 3
beta = "1.00"
nsteps = [20, 10]
for i, p, ns, note in zip(count(), pmdls, nsteps, mlbls):
    x = np.loadtxt(
        f"{p}/Lx{Lx}Ly{Ly}-avqite-beta{beta}-E_v_step.dat", usecols=0)
    y = np.loadtxt(
        f"{p}/Lx{Lx}Ly{Ly}-avqite-beta{beta}-E_v_step.dat", usecols=1)
    yerr = np.loadtxt(
        f"{p}/Lx{Lx}Ly{Ly}-avqite-beta{beta}-E_v_step.dat", usecols=2)

    axs[0, i].errorbar(x, y, yerr=yerr, color=colors[0],
                       fmt=symbols[0], label=rf"$\beta={float(beta)}$",
                       capsize=4.)

    x = [0, ns + 1]
    y = np.loadtxt(f"{p}/Lx{Lx}Ly{Ly}-exact-E_v_beta.dat", usecols=1)
    for j, dum in enumerate(np.loadtxt(f"{p}/Lx{Lx}Ly{Ly}-exact-E_v_beta.dat", usecols=0)):
        if abs(dum - float(beta)) < 1e-12:
            y = 2 * [y[j]]

    axs[0, i].plot(x, y, "--", color="black",
                   label=None)  # rf"\textrm{{exact}}"

    if not i:
        x = np.loadtxt(
            f"{p}/Lx{Lx}Ly{Ly}-avqite-beta1.60-E_v_step.dat", usecols=0)
        y = np.loadtxt(
            f"{p}/Lx{Lx}Ly{Ly}-avqite-beta1.60-E_v_step.dat", usecols=1)
        yerr = np.loadtxt(
            f"{p}/Lx{Lx}Ly{Ly}-avqite-beta1.60-E_v_step.dat", usecols=2)

        axs[0, i].errorbar(x, y, yerr=yerr, color=colors[1],
                           fmt=symbols[1], label=rf"$\beta=1.6$",
                           capsize=4.)

        x = [0, ns + 1]
        y = np.loadtxt(f"{p}/Lx{Lx}Ly{Ly}-exact-E_v_beta.dat", usecols=1)
        for j, dum in enumerate(np.loadtxt(f"{p}/Lx{Lx}Ly{Ly}-exact-E_v_beta.dat", usecols=0)):
            if abs(dum - 1.6) < 1e-12:
                y = 2 * [y[j]]

        axs[0, i].plot(x, y, "--", color="black",
                       label=None)  # rf"\textrm{{exact}}"

    if i == 0:
        axs[0, i].set(
            ylabel=r"$\langle \hat{{\mathcal{{H}}}} \rangle_{\boldsymbol{\theta}(\beta)}$")
    axs[0, i].set(xlabel=rf"\textrm{{thermal step}}")
    axs[0, i].xaxis.set_ticks([j for j in range(0, ns + 1, ns // 5)])
    axs[0, i].yaxis.set_major_locator(MaxNLocator(5))
    axs[0, i].set_xlim(left=0, right=ns + 0.25)
    # axs[0, i].legend(frameon=False, loc='best')

    axs[0, i].annotate(note, xy=(0.3 if i else 0.2, 0.15 if i else 0.05),
                       xycoords='axes fraction', fontsize=tsize)

axs[0, 1].set_yticks([-52.27, -52.25, -52.23])


# axs[0, i].annotate(rf"$\beta={float(beta)}$", xy=(0.55, 0.6),
#                    xycoords='axes fraction')

axs[0, 0].annotate(r"$\beta=1.0$", xy=(0.65, 0.58),
                   xycoords='axes fraction')

axs[0, 0].annotate(r"$\beta=1.6$", xy=(0.6, 0.1),
                   xycoords='axes fraction')

axs[0, 0].annotate(r"$N=4\times3$", xy=(0.25, 0.45),
                   xycoords='axes fraction')

xlist = [1] * len(energy_levels_tfim)
axins[2].scatter(xlist, energy_levels_tfim, marker="_", s=1000, linewidth=0.5)
xlist = [4] * len(energy_levels_mfim)
axins[2].scatter(xlist, energy_levels_mfim, marker="_", s=1000, linewidth=0.5)
axins[2].axhline(y=1, color='r', ls=':')
axins[2].annotate(r"$\frac{1}{\beta}=1$", xy=(0.55, 0.2),
                  xycoords='axes fraction', fontsize=14)
axins[2].set_ylim(-0.5, 12.2)
axins[2].set_xlim(-1, 6)
axins[2].set_xticks([1, 4])
axins[2].set_yticks([0, 3, 6, 9, 12])
axins[2].set_xticklabels(['TFIM', 'MFIM'])

for i, p in zip(count(), pmdls):
    x = np.loadtxt(f"{p}/Lx{Lx}Ly{Ly}-avqite-E_v_beta.dat", usecols=0)
    y = np.loadtxt(f"{p}/Lx{Lx}Ly{Ly}-avqite-E_v_beta.dat", usecols=1)
    yerr = np.loadtxt(f"{p}/Lx{Lx}Ly{Ly}-avqite-E_v_beta.dat",
                      usecols=2) / np.sqrt(len(y))

    axs[1, i].errorbar(x, y, yerr=yerr, color=colors[0],
                       fmt=symbols[0], label=None,
                       capsize=4.)

    y_exct = np.loadtxt(f"{p}/Lx{Lx}Ly{Ly}-exact-E_v_beta.dat", usecols=1)
    y = abs(y - y_exct) / abs(y_exct)
    axins[i].semilogy(x, y, symbols[1], color=colors[1],
                      label=None)

    if i == 0:
        axs[1, i].set(
            ylabel=r"$\langle \hat{{\mathcal{{H}}}} \rangle_{\boldsymbol{\theta}(\beta)}$")
    axs[1, i].set(xlabel=rf"$\beta$")
    axs[1, i].yaxis.set_major_locator(MaxNLocator(5))
    axs[1, i].xaxis.set_ticks([0.4, 0.8, 1.2, 1.6])
    axs[1, i].set_xlim(right=1.65)
    axins[i].xaxis.set_ticks([0.4, 0.8, 1.2, 1.6])
    axins[i].yaxis.set_ticks([1e-2, 1e-4, 1e-6])
    # axins[i].minorticks_off()
    # axins[0].yaxis.set_label_position("right")
    # axins[0].yaxis.set_ticks_position("right")
    # ip = InsetPosition(axs[1, 0], [0.2, 0.45, 0.5, 0.5]
    #                    )  # posx, posy, width, height
    # axins[0].set_axes_locator(ip)
    axins[i].set_ylabel(r'$\epsilon$')
    axins[i].tick_params(labelsize=11)
    # ylabel=rf"$\left\langle \hat{{\mathcal{{H}}}} \right\rangle$"
    # rf"$- \left\langle \hat{{\mathcal{{H}}}} \right\rangle_\textrm{{ED}}$")


xsizes = [2, 3, 3, 4, 4]
ysizes = [2, 2, 3, 3, 4]
labels_cx = [rf"$\beta=0.4$", rf"$\beta=0.8$"]
betas = [0.4, 0.8]
symbols = symbols[:len(betas)]
colors = colors[:len(betas)]
for cnt, p, m in zip(count(), pmdls, mlbls):
    for dum, basis in enumerate(["zbasis", "xbasis"]):
        xx = np.empty((len(betas), len(xsizes)))
        yy = np.empty((len(betas), len(xsizes)))
        yyerr = np.empty((len(betas), len(xsizes)))
        for j, Lx, Ly in zip(count(), xsizes, ysizes):
            x = np.loadtxt(
                f"{p}/Lx{Lx}Ly{Ly}-avqite-Ncx_v_beta-{basis}.dat", usecols=0)
            y = np.loadtxt(
                f"{p}/Lx{Lx}Ly{Ly}-avqite-Ncx_v_beta-{basis}.dat", usecols=1)
            yerr = np.loadtxt(
                f"{p}/Lx{Lx}Ly{Ly}-avqite-Ncx_v_beta-{basis}.dat", usecols=2)
            for i, b in enumerate(betas):
                pos = [k for k, dum in enumerate(x) if np.isclose(dum, b)][0]
                xx[i, j] = Lx * Ly
                yy[i, j] = y[pos]
                yyerr[i, j] = yerr[pos]

        for i, sym, clr in zip(count(), symbols, colors):
            if dum:
                lbl = None if cnt else labels_cx[i]
                axs[2, cnt].errorbar(xx[i, :], yy[i, :], yerr=yyerr[i, :],
                                     fmt=sym, color=clr, label=lbl, capsize=4.)
            else:
                lbl = None
                sym += '-'
                axs[2, cnt].errorbar(xx[i, :], yy[i, :], yerr=yyerr[i, :],
                                     fmt=sym, color=clr, label=lbl, capsize=4.)

    axs[2, cnt].set(xlabel=r'$N$')
    if cnt == 0:
        axs[2, cnt].set(ylabel=r'$N_\text{cx}$')
    axs[2, cnt].yaxis.set_major_locator(MaxNLocator(5))
    axs[2, cnt].xaxis.set_ticks([x * y for x, y in zip(xsizes, ysizes)])
    axs[2, cnt].legend(frameon=False, loc='best')

axs[2, 1].annotate(r"$X\textrm{-step}$", xy=(0.45, 0.72),
                   xycoords='axes fraction')
axs[2, 1].annotate(r"$Z\textrm{-step}$", xy=(0.55, 0.32),
                   xycoords='axes fraction')

axs[0, 0].annotate("(a)", xy=(-0.35, 0.95),
                   xycoords='axes fraction')
axs[0, 1].annotate("(b)", xy=(-0.25, 0.95),
                   xycoords='axes fraction')
axs[1, 0].annotate("(c)", xy=(-0.35, 0.95),
                   xycoords='axes fraction')
axs[1, 1].annotate("(d)", xy=(-0.25, 0.95),
                   xycoords='axes fraction')
axs[2, 0].annotate("(e)", xy=(-0.35, 0.95),
                   xycoords='axes fraction')
axs[2, 1].annotate("(f)", xy=(-0.25, 0.95),
                   xycoords='axes fraction')

plt.savefig('plt_E_and_Ncx_v_beta-2D.pdf')

plt.show()
